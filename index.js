import express, { json, urlencoded } from "express";
import { HttpStatus, port, secretKey } from "./src/config/config.js";
import connectDb from "./src/connectdb/connectdb.js";
import { firstRouter } from "./src/router/firstRouter.js";
import { foodRouter } from "./src/router/foodRoute.js";
import { secondRouter } from "./src/router/secondRouter.js";
import { Schema, model } from "mongoose";
import studentRouter from "./src/router/studentRouter.js";
import playerRouter from "./src/router/playerRouter.js";
import errorHandler from "./src/helper/errorHandler.js";
import productRouter from "./src/router/productRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import { sendMail } from "./src/utils/sendMail.js";
import fileRouter from "./src/router/fileRouter.js";
import bcrypt from "bcrypt";
import adminRouter from "./src/router/adminRouter.js";
import jsonwebtoken from "jsonwebtoken";

let expressApp = express();
// expressApp.use((req, res, next) => {
//   console.log("i am first");
//   next();
// });

// expressApp.use((req, res, next) => {
//   console.log("i am second");
//   next({ name: "nitan" });
// });
expressApp.use(json()); //it is used to get json data put this cod at top so that all route can use req.body
expressApp.use(urlencoded({ extended: true })); //it is used to get form data
expressApp.use(express.static("./public"));
// expressApp.use(express.static("./p1"));
// we can make more than one static folder (dont make file as static)
//if a folder is static we can access the file in that folder
expressApp.use("/first", firstRouter);
expressApp.use("/second", secondRouter);
expressApp.use("/food", foodRouter);
expressApp.use("/students", studentRouter);
expressApp.use("/players", playerRouter);
expressApp.use("/products", productRouter);
expressApp.use("/reviews", reviewRouter);
expressApp.use("/uploads", fileRouter);
expressApp.use("/admins", adminRouter);
// expressApp.use((req, res, next) => {
//   console.log("i am 4");
//   next("rrrrrr");
// });

// to sendMail just call
// try {
//   await sendMail({
//     from: '"Fred Foo" <nitanthapa425@gmail.com>',
//     to: [
//       "nitanthapa425@gmail.com",
//       "adhikariluffy20@gmail.com",
//       "hipakdev@gmail.com",
//     ],
//     subject: "this is subject",
//     html: `<h1>Hello World<h1>`,
//   });
//   console.log("email is sent successfully");
// } catch (error) {
//   console.log("unable to send email");
// }

expressApp.use(errorHandler); //always put middleware at last.

// expressApp.use((err, req, res, next) => {
//   let statusCode = err.statusCode || HttpStatus.INTERNAL_SERVER_ERROR;
//   let message = err?.message || "Internal server error";
//   res.status(statusCode).json({
//     success: false,
//     message,
//   });
// });

// token = info + secretkey;
//generate token

let info = { _id: "123434", name: "" };
let expiryInfo = {
  expiresIn: "30s",
};

// let token = await jsonwebtoken.sign(info, "secretkey", expiryInfo);
// console.log(token);
// let token = await generateToken({
//   info: info,
//   secretKey: secretKey,
//   expiryInfo: expiryInfo,
// });
// console.log(token);

// //verify token
// // let _info = await jsonwebtoken.verify(token, "secretkey");
// // console.log(_info);

// let _info = await verifyToken({ token: token, secretKey: secretKey });
// console.log(_info);

connectDb();

expressApp.listen(port, () => {
  console.log(`express app is listening at port ${port}`);
});
