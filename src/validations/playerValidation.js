import Joi from "joi";
const playerValidation = Joi.object()
  .keys({
    // name: Joi.string().required()
    //to use regex with custom message,

    name: Joi.string().custom((value, msg) => {
      if (value.match(/^[a-zA-Z]+$/)) {
        return true;
        //return true if you want to pass validation
      } else {
        return msg.message("Only alphabet is allowed");
        // return msg.messge if you want to through custom error
      }
    }),

    // name: Joi.any().required(),//.any means any type
    age: Joi.number().min(18).max(60).required(),
    noOfMatches: Joi.number().required(),
    //if isMarried
    //  is true
    // then spouse is required
    //  otherwis spouse is optional
    isMarried: Joi.boolean().required(),
    // spouse: Joi.string().optional().allow(""),
    spouse: Joi.string().when("isMarried", {
      is: true,
      // is must return true or false
      //if true then code execute
      //else otherwise code execute
      then: Joi.required(),
      otherwise: Joi.optional().allow(""),
    }),
    gender: Joi.string().required().valid("male", "female", "other"),
    parentInfo: Joi.object().keys({
      fatherName: Joi.string().required(),
      motherName: Joi.string().required(),
    }),
    favFood: Joi.array().items(Joi.string()).required(),
    playerGame: Joi.array().items(
      Joi.object().keys({
        matchName: Joi.string().required(),
        noOfGoal: Joi.number().required(),
      })
    ),
  })
  .unknown(false);

export default playerValidation;
