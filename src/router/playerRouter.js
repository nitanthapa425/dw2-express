import { Router } from "express";
import {
  createPlayer,
  deletePlayer,
  readAllPlayers,
  readPlayerDetails,
  updatePlayerDetails,
} from "../controller/playerController.js";

import Joi from "joi";
import validation from "../middleware/validation.js";
import playerValidation from "../validations/playerValidation.js";
import { verifyToken } from "../utils/token.js";
import expressAsyncHandler from "express-async-handler";
import { secretKey } from "../config/config.js";
import isAuthenticated from "../middleware/isAuthenticated.js";
import isAuthorized from "../middleware/isAuthorized.js";

const playerRouter = Router();

// create schema for joi
// let player = {
//   name: "nitan",
//   age: 425,
//   noOfMatches: 3,
//   isMarried: true,
//   spouse: "puja",
//   gender: "male",
//   parentInfo: { fatherName: "aaaa", motherName: "bbb" },
//   playerGame: [
//     { matchName: "A", noOfGoal: 3 },
//     { matchName: "A", noOfGoal: 3 },
//   ],
//   favFood: ["chicken", "mutton", "fish"],
//   abc: [1, 23, 3, 4],

// };

playerRouter
  .route("/")
  // .post(validation(playerValidation), createPlayer)
  .post(isAuthenticated, isAuthorized(["admin", "superAdmin"]), createPlayer)

  .get(isAuthenticated, isAuthorized(["admin", "superAdmin"]), readAllPlayers);

playerRouter
  .route("/:id")
  .get(
    isAuthenticated,
    isAuthorized(["admin", "superAdmin"]),
    readPlayerDetails
  )
  .patch(
    isAuthenticated,
    isAuthorized(["admin", "superAdmin"]),
    validation(playerValidation),
    updatePlayerDetails
  )
  .delete(isAuthenticated, isAuthorized(["admin", "superAdmin"]), deletePlayer);

export default playerRouter;
