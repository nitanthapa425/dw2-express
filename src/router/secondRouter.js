import { Router } from "express";

export const secondRouter = Router();

// main url
// localhost:8000/second/b

//not * position matter so it is recommmended to put at last

secondRouter.route("/a").get((req, res) => {
  res.json("I am second route a");
});

secondRouter.route("/a/:id").post((req, res) => {
  //to get

  //to get route params use req.params which is object
  console.log(req.params.id);
  //to get query params use req.query which is object
  console.log(req.query.name);

  console.log(req.query.age);
  res.json(`I am second route ${req.params.id} `);
});
secondRouter.route("/a/:id/:name").get((req, res) => {
  //to get
  console.log(req.params.id);
  console.log(req.params.name);
  res.json(`id is ${req.params.id} and name is ${req.params.name}`);
});

secondRouter.route("/*").get((req, res) => {
  res.json(`i am *`);
});
