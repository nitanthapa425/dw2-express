import { Router } from "express";
import {
  createStudent,
  deleteStudent,
  readAllStudents,
  readStudentDetails,
  updateStudentDetails,
} from "../controller/studentController.js";
import validation from "../middleware/validation.js";
import studentValidation from "../validations/studentValidation.js";

const studentRouter = Router();
// name: {
//   type: String,
//   required: [true, "Name field is required"],
// },
// roll: {
//   type: Number,
//   required: [true, "Roll field is required"],
// },
// address: {
//   type: String,
//   required: [true, "Address field is required"],
// },

studentRouter
  .route("/")
  .post(validation(studentValidation), createStudent)
  .get(readAllStudents);

studentRouter
  .route("/:id")
  .get(readStudentDetails)
  .patch(updateStudentDetails)
  .delete(deleteStudent);

export default studentRouter;
