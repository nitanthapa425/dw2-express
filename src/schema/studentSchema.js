import { Schema } from "mongoose";

//defind object =>Schema
let studentSchema = Schema(
  {
    name: {
      type: String,
      required: [true, "Name field is required"],
    },
    roll: {
      type: Number,
      required: [true, "Roll field is required"],
    },
    address: {
      type: String,
      required: [true, "Address field is required"],
    },
  },
  {
    timestamps: true,
  }
);
export default studentSchema;
