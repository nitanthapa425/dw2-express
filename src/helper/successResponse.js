import { HttpStatus } from "../config/config.js";

// while making function it is good to pass data in object formate to avoid order
const successResponse = ({ result, status, res, message = "successful" }) => {
  let _result = result || null;
  let _status = status || HttpStatus.OK;
  res.status(_status).json({
    success: true,
    result: _result,
    message: message,
  });
};
export default successResponse;
// successResponse({
//   res,
//   status: HttpStatus.OK,
//   success: true,
//   result,
// });
// successResponse({
//   res,
//   status: HttpStatus.OK,
//   success: true,
//   result,
// });

// successResponse({
//   res,
//   status: HttpStatus.OK,
//   result,
// });
