import expressAsyncHandler from "express-async-handler";
import { HttpStatus } from "../config/config.js";

let isAuthorized = (validRoles) => {
  return expressAsyncHandler(async (req, res, next) => {
    console.log(req.info);

    let _isAuthorized = validRoles.includes(req.info.role);
    if (_isAuthorized) {
      next();
    } else {
      let err = new Error("Unauthorized user");
      err.statusCode = HttpStatus.FORBIDDEN;
      throw err;
    }
  });
};

export default isAuthorized;
